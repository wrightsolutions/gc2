projects = {
    'dev': 'gdev',
    'test': 'gtest',
    'stage': 'gstage',
    'prod': 'gprod',
}
clusters = {
    'dev': 'k8d',
    'test': 'k8t',
    'stage': 'k8s',
    'prod': 'k8p',
}

