#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2019 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from sys import argv,exit
from datetime import datetime as dt
import shlex
from subprocess import Popen,PIPE
import project_cluster as pc

# python ./kc.py dev topnode

TWO_TO_SIXTEEN = 2**16
# chr(44) is comma, chr(58) is colon (:)
BUFSIZE=4096		#BUFSIZE=1024
SLEEP_FOR=2
KUBECTL_PREFIXED="sleep {0}; kubectl".format(SLEEP_FOR)


def ip4valid(ipv4string):
	""" Returns stripped ip4 if a valid IP version 4 address
	otherwise returns None
	"""	
	addr_return = None
	addr_stripped = ipv4string.strip()
	try:
		socket.inet_aton(addr_stripped)
		addr_return = addr_stripped
	except:
		pass
	return addr_return


def validport(port_given,port_minimum=1024):
	port_int = None
	try:
		port_int = int(port_given)
	except:
		pass
	if port_int >= TWO_TO_SIXTEEN:
		# Above maximum allowed value for port
		return None
	elif port_int < 1:
		# Too small
		return None
	elif port_int < port_minimum:
		# Below limit based on supplied arg/defaul
		return None
	else:
		# Valid port passing all limit tests
		pass

	return port_int


def sub5(sub_command,verbosity=0):
	""" Execute subprocess popen for the given command
	Return a 4 tuple of stdout as iterator, stdout, stderr, and return code
	"""
	cmdlex = shlex.split(sub_command)
	subbed = Popen(cmdlex,bufsize=BUFSIZE,stdout=PIPE,stderr=PIPE)
	subout, suberr = subbed.communicate()
	# .returncode is only populated after a communicate() or poll/wait
	subrc = subbed.returncode
	if verbosity > 1 and subrc > 2:
		print("Error {0} during {1}".format(ncon_rc,cmd_ncon))
	elif verbosity > 2:
		if subrc > 0:
			print("subrc={0} from .returncode={1}".format(subrc,subbed.returncode))
	else:
		pass
	subout_iter = iter(subout.splitlines())
	try:
		suberr_iter = iter(suberr.splitlines())
	except AttributeError:
		suberr_iter = iter([])
	return (subout_iter,subout,suberr,suberr_iter,subrc)


def prompt_from_arg(arg_given):
	prompt = None
	argstring = arg_given.strip()
	if argstring in ['topnode','toppod']:
		if argstring.endswith('node'):
			prompt = 'select Node'
		elif argstring.endswith('pod'):
			prompt = 'select Pod'
		elif argstring.endswith('machine'):
			prompt = 'select Machine'
		else:
			pass
	return prompt


def select_from_sub(cmd_given):
	selected = None
	outiter, subout, suberr, erriter, subrc = sub5(cmd_given,verbosity=0)
	print(cmd_given)
	select_lines = []
	if 0 == subrc:
		selected = 'blah'
		idx = 0
		while True > False:
			idx+=1
			try:
				line = outiter.next()
				select_lines.append("{0} {1}".format(idx,line.lstrip().split()[0]))
			except StopIteration:
				break
		for line in select_lines:
			print(line)
		if len(select_lines) < 1:
			pass
		elif 1 == len(select_lines):
			selected = select_lines[1].split()[1]
		else:
			sel = input("Enter your selection (as number):")
			selected = select_lines[sel-1].split()[1]
	else:
		print("kubectl get failed!")
	print(selected)
	return selected


def kline_from_selector(selector,cluster=None):

	prompt = prompt_from_arg(selector)

	kline = None
	if prompt is None:
		if selector is None:
			pass
		elif selector.startswith('get-'):
			kline = "config {0}".format(selector)
		elif selector.startswith('set-'):
			kline = "config {0}".format(selector)
		else:
			pass
		if kline is None:
			kline = selector
	elif prompt.startswith('select'):
		kuse = "kubectl config use-context {0}".format(cluster)
		if cluster is None or len(cluster) < 2:
			pass
		elif prompt.endswith('ode'):
			kline = "get nodes -o wide"
			kcmd = "{0}; {1} {2}".format(kuse,KUBECTL_PREFIXED,kline)
			selected = select_from_sub(kcmd)
			if selected is None:
				kline = None
			else:
				kline = selected
		elif prompt.endswith('Pod'):
			kline = "get pods -o wide"
			kcmd = "{0}; {1} {2}".format(kuse,KUBECTL_PREFIXED,kline)
			selected = select_from_sub(kcmd)
			if selected is None:
				kline = None
			else:
				kline = selected
		elif prompt.endswith('Machine'):
			kline = "get machines -o wide"
			kcmd = "{0}; {1} {2}".format(kuse,KUBECTL_PREFIXED,kline)
			selected = select_from_sub(kcmd)
			if selected is None:
				kline = None
			else:
				kline = selected
		else:
			print("kubctl get [unknown]!")
			#pass
	else:
		pass

	return kline


if __name__ == '__main__':

	program_binary = argv[0].strip()
	dtiso = dt.isoformat(dt.now())

	env = argv[1]
	proj = None
	cluster = None
	if env is not None and env in pc.projects:
		try:
			proj = pc.projects[env]
			cluster = pc.clusters[env]
		except:
			proj = "gc-{0}".format(env)
	else:
		env = 'test'
		proj = 'gc-test'

	print("# {0} {1}<->{2}".format(dtiso,env,proj))

	arg2 = argv[2]
	selector = arg2.strip()
	prompt = None
	if selector == 'info':
		kline = 'cluster-info'
	elif selector == 'dump':
		kline = 'cluster-info dump'
	elif selector == 'view':
		kline = 'config-view'
	else:
		kline = kline_from_selector(selector,cluster)

	kuse = "kubectl config use-context {0}".format(cluster)
	if kline is None:
		print("## {0};".format(kuse))
	elif selector == 'use-context':
		# No need for any kline as 'use-context' is all we need here.
		print(kuse)
	else:
		kcmd = "{0}; {1} {2}".format(kuse,KUBECTL_PREFIXED,kline)
		print(kcmd)


	exit(0)
	
#	port1 = validport(argv[2])
#	ip_after_translate = ip4valid(argv[3])


